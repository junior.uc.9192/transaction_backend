from django.conf.urls import url, include
from rest_framework import routers
from transaction.trackers import views

router = routers.DefaultRouter()
router.register(r'category', views.CategoryViewSet)
router.register(r'transaction', views.TransactionViewSet)

urlpatterns = [
    url(r'^', include(router.urls))
]