from datetime import datetime
from datetime import timedelta
from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django_filters import rest_framework as filters
from transaction.trackers import models

class StandardFilters(filters.FilterSet):
    date = filters.DateFromToRangeFilter()

    class Meta:
        model = models.Transaction
        fields = ['date', 'category', '_type']


class TransactionDateListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('Date')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'decade'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('week', _('last seven days')),
            ('month', _('last thirty days')),
            ('year', _('current year'))
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value() == 'week':
            last_week = datetime.now() - timedelta(days=7)
            return queryset.filter(date__gte=last_week)
        
        if self.value() == 'month':
            last_month = datetime.now() - timedelta(days=30)
            return queryset.filter(date__gte=last_month)

        if self.value() == 'year':
            return queryset.filter(date__year=datetime.now().year)