from rest_framework import serializers
from transaction.trackers import models


class CategorySerializer(serializers.ModelSerializer):
    """
    Serializer definition for Category Model.
    """
    class Meta:
        model = models.Category
        fields = '__all__'


class TransactionSerializer(serializers.ModelSerializer):
    """
    Serializer definition for Transaction Model.
    """
    category = CategorySerializer(read_only=True)
    categoryId = serializers.PrimaryKeyRelatedField(
        write_only=True, 
        queryset=models.Category.objects.all(), 
        source='category'
        )
    type = serializers.CharField(source='get__type_display', read_only=True)
    _type = serializers.ChoiceField(write_only=True, choices=models.Transaction.TRANSACTION_TYPE)

    class Meta:
        model = models.Transaction
        fields = '__all__'