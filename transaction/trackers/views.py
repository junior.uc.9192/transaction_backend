from rest_framework import viewsets

from transaction.trackers import serializers, models
from django_filters.rest_framework import DjangoFilterBackend
from transaction.trackers.filters import StandardFilters


# VIEWS API REST

class CategoryViewSet(viewsets.ModelViewSet):
    """
    ViewSet definition for Category Model.

    list:
    Returns list of all categories.

    create:
    Create a new instance.

    retrieve:
    Returns a given category.

    update:
    Update a given category.

    delete:
    Delete a given category
    """
    queryset = models.Category.objects.all()
    serializer_class = serializers.CategorySerializer


class TransactionViewSet(viewsets.ModelViewSet):
    """
    ViewSet definition for Transaction Model.

    list:
    Returns list of all transactions.

    create:
    Create a new instance.

    retrieve:
    Returns a given transaction.

    update:
    Update a given transaction.

    delete:
    Delete a given transaction
    """
    queryset = models.Transaction.objects.all()
    serializer_class = serializers.TransactionSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = StandardFilters