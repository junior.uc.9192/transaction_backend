from django.contrib import admin
from transaction.trackers import models
from daterange_filter.filter import DateRangeFilter

# Register your models here.

@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    fields = ['name']


@admin.register(models.Transaction)
class TransactionAdmin(admin.ModelAdmin):
    readonly_fields = ['date']
    list_display = ['subject', 'category', 'amount', 'date', '_type']
    list_filter = [('date', DateRangeFilter), 'category__name', '_type']