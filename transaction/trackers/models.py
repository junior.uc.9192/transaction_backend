from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.


class Category(models.Model):
    """
    Model definition for Category.
    """
    name = models.CharField(max_length=128, help_text=_("Name"))

    class Meta:
        """Meta definition for Platform."""
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Transaction(models.Model):
    """
    Model definition for Transaction.
    """
    WITHDRAWAL = 0
    DEPOSIT = 1
    TRANSACTION_TYPE = (
        (WITHDRAWAL, _("Withdrawal")),
        (DEPOSIT, _("Deposit"))
    )
    subject = models.CharField(max_length=128, help_text=_("Subject"))
    category = models.ForeignKey("Category", on_delete=models.CASCADE, help_text=_("Category"))
    amount = models.FloatField(help_text=_("Transaction Amount"))
    date = models.DateTimeField(
        auto_now_add=True, 
        verbose_name=_("Date"),
        help_text=_("Date")
        )
    _type = models.PositiveIntegerField(
        choices=TRANSACTION_TYPE,
        default=WITHDRAWAL, 
        verbose_name=_("Type"), 
        help_text=_("Transaction type")
        )

    class Meta:
        """Meta definition for Transaction."""
        verbose_name = 'Transaction'
        verbose_name_plural = 'Transactions'

    def __str__(self):
        return "%s - %s" %(self.subject, self._type)